﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circleMovmentScr : MonoBehaviour {
    float timeCounter = 0;
    public float size = 2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timeCounter += Time.deltaTime;

        float x = timeCounter*(size/2);
        float y = Mathf.Sin(timeCounter)*size;

        transform.position = new Vector3(x, y, 0);
		
	}
}
