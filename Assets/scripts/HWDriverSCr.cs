﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HWDriverSCr : MonoBehaviour {
    public GameObject drone;
    public GameObject droneNeighbour;
    public GameObject graphTic;
    public List<GameObject> DroneList;
    public List<GameObject> droneNeighbourList;
    GameObject UIDroneList;
    Vector3 origanDroneListing;
    float k = 1.2f,
       d = 15;

    // Use this for initialization
    void Start () {
        origanDroneListing = droneNeighbour.GetComponent<RectTransform>().localPosition;
        UIDroneList = GameObject.Find("DroneLists");
        DroneList = new List<GameObject>();
        droneNeighbourList = new List<GameObject>();
        for (int i = 0; i < 100; i++)
        {
            //random drones
            GameObject temp = Instantiate(drone, 
                new Vector3(Random.Range(1.0f, 180.0f), Random.Range(1.0f, 180.0f), 0), Quaternion.identity);
            temp.name = "D" + i.ToString();
            temp.GetComponent<HW1DroneScr>().myIndex = i;
            DroneList.Add(temp);

            //graph markers

           

            // list of neighbours in text format under UI
            GameObject temp2 = Instantiate(droneNeighbour);
            temp2.transform.parent = UIDroneList.transform;
            temp2.GetComponent<RectTransform>().localPosition = origanDroneListing;
            droneNeighbourList.Add(temp2);
            origanDroneListing = new Vector3(origanDroneListing.x, origanDroneListing.y - 20, origanDroneListing.z);
            
        }

        for(int i = 0; i < 180; i++ )
        {
            if (i % 5 == 0)
            {
                //y
                GameObject tempTic = Instantiate(graphTic, new Vector3(i + 1, -5, 0), Quaternion.identity);
                tempTic.transform.Rotate(0, 0, 90);
                //x
                Instantiate(graphTic, new Vector3(-5, i + 1, 0), Quaternion.identity);
                
            }
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void updateNeighbourList(string stuff, int index)
    {
        droneNeighbourList[index].GetComponent<Text>().text = stuff;
    }

    public void connectDrones()
    {
        for (int i = 0; i < 100; i++)
        {
            for (int j = 0; j < 100; j++)
            {
                if (euclidNorm(droneNeighbourList[j].transform.position - droneNeighbourList[i].transform.position) < d * k)
                {
                    droneNeighbourList[i].GetComponent<HW1DroneScr>().addNeighbor(droneNeighbourList[j]);
                }
            }
        }



    }

    public static float euclidNorm(Vector3 postion)
    {
        return Mathf.Sqrt((postion.x * postion.x) + (postion.y * postion.y) + (postion.z * postion.z));
    }


    public void resetGame()
    {
        SceneManager.LoadScene(0);
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
