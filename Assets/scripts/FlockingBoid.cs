﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// this should be attached to all objects flocking
public class FlockingBoid : MonoBehaviour
{

    #region variables

    //list of neighbors 
    public List<GameObject> neighborList; // dynamic list of neighbors
    float thrust = 1f;
    public Vector2 randDirection;
    // max radius of neighbors stolen from the static list on scene load: inherantly
    public float radiusMax = staticFunctionsFlocking.radius;
    GameObject tempGO;
    public GameObject target;

    public LineRenderer mainLine;
    public Material blueLine;
    public Color c1 = Color.blue;
    int stringIndex;
    public List<int> stringIndexing;

    int index;
    int index2 = -1;

    Vector3 source;
    #endregion

    /// <summary>

    /// </summary>
    // Use this for initialization when instantiated
    void Start ()
    {
        neighborList = new List<GameObject>(); // instantiated a list of neighbors on instantiation
        stringIndexing = new List<int>();
        randDirection = new Vector3(Random.Range(-6.0f, 6.0f), Random.Range(-6.0f, 6.0f));
        gameObject.GetComponent<Rigidbody2D>().AddForce(randDirection * thrust);
       // mainLine = GetComponent<LineRenderer>();
        //mainLine.numPositions = 2;
        //mainLine.SetPosition(0, gameObject.transform.position);
        source = gameObject.transform.position;

       
        

    }


    /// <summary>
    /// 
    /// </summary>
    // Update is called once per frame 
    void Update()
    {
        Vector2 AlgOneReturn = staticFunctionsFlocking.FlockingAlgo1(neighborList, gameObject, target);

        // this is where we call the flocking algorithms
        gameObject.GetComponent<Rigidbody2D>().AddForce(AlgOneReturn);

        // add constant force
        //gameObject.GetComponent<Rigidbody2D>().AddForce(randDirection * thrust);


        //draw lines 

        //setEdge(source, gameObject.transform.position, 0);
        // for every line on the robot
        /*
        for (int i = 0; i < 30; i++)
        {
            for (int j = 0; j < stringIndexing.Count; j++)
            {
                if (i != stringIndexing[j])
                {
                    setEdge(gameObject.transform.position, gameObject.transform.position, i);
                }
            }
       
        }

        for( int i = 0; i < neighborList.Count; i++)
        {
            setEdge(gameObject.transform.position, neighborList[i].transform.position, stringIndexing[i]);
        }
        */

    }

    ///////////////////////////////////////////////////////////////////////////

    // boid movement functions
    public void boidBasicMovement()
    {

        // this will either change the position of the transform or push the object using the target and the physics engine.

    }
    // target following
    public void followTarget()
    {
        // function to 
    }
    // 

    void OnTriggerEnter2D(Collider2D newNeighbor)
    {
        if (neighborList.Find(x=> x == newNeighbor.gameObject)  == null)
        {
            neighborList.Add(newNeighbor.gameObject);
           
            stringIndexing.Add(((neighborList.Count-1)*2));
            
        }
    }

    void OnTriggerStay2D(Collider2D newNeighbor)
    {
        if (neighborList.Find(x => x == newNeighbor.gameObject) != null)
        {
            int index = neighborList.FindIndex(x => x == newNeighbor.gameObject);

        }
    }

    void OnTriggerExit2D(Collider2D newNeighbor)
    {
        
        tempGO = neighborList.Find(x => x == newNeighbor.gameObject);
        int index = neighborList.FindIndex(x => x == newNeighbor.gameObject);
        
        if ( tempGO != null)
        {
            neighborList.Remove(tempGO);
            stringIndexing.RemoveAt(index);
        }
    }

    public void setEdge(Vector3 source, Vector3 dest, int lineIndex)
    {
        //mainLine.material = new Material(Shader.Find("Particles/Additive"));
        mainLine.startColor = c1;
        mainLine.endColor = c1;

        
        mainLine.startWidth = 0.03f;
        mainLine.endWidth = 0.03f;
        mainLine.SetPosition(lineIndex, source);
        lineIndex++;
        mainLine.SetPosition(lineIndex, dest);

    }





}
