﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public static class staticFunctionsFlocking
{
    // variables
    #region variables
    
    public static Vector3          tmpV3;
    public static Vector2          tmpV2;
    public static float            tmpFloat;
    public static float            h = 0.8f; // positionRatio = euclideanNorm/radius - Where we want the robot 

    // for ease im adding the radius max
    public static float desiredDistance;
    // and desired distance into this file

    public static float radius = 2.5f;

    public static float aPhi = 19;
    public static float bPhi = 20;

    public static float Econst = 0.008f;

    public static float targetConst = 0.2f;

    #endregion


    // algorithm 2 : #23
    #region flocking Algorithm #2 : ui = ui_alpha + ui_gamma
    public static Vector2 FlockingAlgo2(Vector2 gradTerm, Vector2 velTerm, Vector2 tarTerm)
    {
        // this is the main function for the flocking without collision avoidance
        return gradTerm + velTerm + tarTerm;
               
    }
    #endregion

    #region flocking Algorithm #1
    public static Vector2 FlockingAlgo1(List<GameObject> neighbor, GameObject currentRob, GameObject target)
    {
        Vector2 gradientTerm = new Vector2();
        Vector2 consensusTerm = new Vector2();

        //call function #15 for all neighbors 
        for (int i = 0; i < neighbor.Count; i++)
        {
            tmpV2 =  neighbor[i].transform.position - currentRob.transform.position;
                        
            gradientTerm += phiAlpha(euclidNorm(tmpV2), sigmaNorm(tmpV2)) * Nij(currentRob.transform.position , neighbor[i].transform.position);
            
            //start here
            consensusTerm += aij(currentRob.transform.position, neighbor[i].transform.position) *
                                    (neighbor[i].GetComponent<Rigidbody2D>().velocity - currentRob.GetComponent<Rigidbody2D>().velocity);
           
        }
        Vector2 tempPosUs = new Vector2(currentRob.transform.position.x, currentRob.transform.position.y);
        Vector2 tempPosTarget = new Vector2(target.transform.position.x, target.transform.position.y);
        return gradientTerm + consensusTerm/10 + targeting(tempPosUs, currentRob.GetComponent<Rigidbody2D>().velocity,
                                                                     tempPosTarget, target.GetComponent<Rigidbody2D>().velocity);
    }
    #endregion

    // gamma term # 
    #region ui_gamma term calculator
    public static Vector2 ui_gamma( Vector2 objectPos, Vector2 objectVel, Vector2 targetPos, Vector2 targetVel )
    {
        // constants
        float c1 = 1;
        float c2 = 1;


        return (-c1 * (objectPos - targetPos)) + (-c2 * (objectVel - targetVel));

        
    }
    #endregion

    #region gradient term
    public static Vector2 gradientTermCalculator( List<GameObject> neighbors, GameObject myPos )
    {
        
        return tmpV3;
    }
    #endregion

    #region velocity term #
    public static Vector2 velocityTermCalculator()
    {
        

        return tmpV3;
    }
    #endregion

    #region aij - Done
    public static float aij(Vector2 currentRob, Vector2 currentNeighbor)
    {
        return rowH(sigmaNorm(currentNeighbor - currentRob) / radius);
    }
    #endregion

    #region Nij - Done
    public static Vector2 Nij(Vector2 currentRob, Vector2 currentNeighbor)
    {

        return (currentNeighbor - currentRob) / (1 + Econst * sigmaNorm(currentNeighbor - currentRob));
    }
    #endregion

    #region EuclideanNorm - Done
    public static float euclidNorm(Vector2 postion)
    {
        return Mathf.Sqrt((postion.x * postion.x) + (postion.y * postion.y) );
    }
    #endregion

    #region Sigma Norms #8 - Done
    public static float sigmaNorm(Vector2 pos)
    {
        return (Mathf.Sqrt(1 + (Econst*(euclidNorm(pos)* euclidNorm(pos)))) - 1) / (Econst);
    }
    #endregion

    public static float sigmaNormfloat(float dist)
    {
        return (Mathf.Sqrt(1 + (Econst * (dist * dist))) - 1) / (Econst);
    }

    #region phi alpha #15 - Done
    public static float phiAlpha(float euclideanNorm, float sigmaNorm)
    {
        

        return rowH(euclideanNorm / radius) * phi(euclideanNorm - sigmaNorm); 
    }
    #endregion

    #region phi - Done
    public static float phi(float diffEucSig)
    {

        float tmpC;
        // Matt_S: I think these should be moved to variables with h.

        tmpC = (Mathf.Abs(aPhi - bPhi)) / (Mathf.Sqrt(4 * aPhi * bPhi));
        return (0.5f) * ((aPhi + bPhi) * (sigmaOne(diffEucSig + tmpC)) + (aPhi - bPhi));
    }
    #endregion

    #region row h - Done
    public static float rowH(float positionRatio) // positionRatio = euclideanNorm/radius
    {
        if (positionRatio >= 0 && positionRatio < h)
        {
            tmpFloat = -1;
        }
        else if (positionRatio >= h && positionRatio <= 1)
        {
            tmpFloat = 0.5f * (1 + Mathf.Cos(Mathf.PI * (positionRatio - h) / (1 - h)));
        }
        else
        {
            tmpFloat = 0;
        }
        return tmpFloat;
    }
    #endregion

    #region sigmaOne - Done
    public static float sigmaOne(float euclideanNorm)
    {
        return euclideanNorm / (Mathf.Sqrt(1 + (euclideanNorm * euclideanNorm)));         
    }
    #endregion

    public static Vector2 targeting(Vector2 currentBotPos, Vector2 currentBotVel, Vector2 targetPos, Vector2 targetVel)
    {
        return ((-targetConst * currentBotPos) - (-targetConst * targetPos)) - ((targetConst * currentBotVel) - (targetConst * targetVel));
    }
}
