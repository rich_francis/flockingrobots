﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlockingBold3D : MonoBehaviour {

    #region variables

    //list of neighbors 
    public List<GameObject> neighborList; // dynamic list of neighbors

    float thrust = 0.5f;

    // max radius of neighbors stolen from the static list on scene load: inherantly
    public float radiusMax = staticFunctionsFlocking.radius;

    public GameObject target;

    GameObject tempGO;

    #endregion

    /// <summary>

    /// </summary>
    // Use this for initialization when instantiated
    void Start()
    {
        neighborList = new List<GameObject>(); // instantiated a list of neighbors on instantiation
        target = GameObject.Find("Target");
    }


    /// <summary>
    /// 
    /// </summary>
    // Update is called once per frame 
    void Update()
    {
        Vector3 AlgOneReturn = staticFunctionsFlocking3D.FlockingAlgo1(neighborList, gameObject, target);
        Debug.Log(AlgOneReturn);
        // this is where we call the flocking algorithms
        gameObject.GetComponent<Rigidbody>().AddForce(AlgOneReturn);


        //gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * thrust);
        if(Time.time % 60 == 0)
        {
            Debug.Log("second");
            clearNeighborList();
        }
    }

    ///////////////////////////////////////////////////////////////////////////

    
    // this function is used to remove all gameobjects from the neighborList
    void clearNeighborList()
    {
        for( int i = 0; i < neighborList.Count; i++ )
        {
            if(neighborList[i] == null)
            {
                Debug.Log("removing neighbor at :" + i);
                neighborList.RemoveAt(i);
            }
        }
    }

    // when a gameobject with the tag robot enters this robots collider sphere 
    // that robot is added to the robots neighbor list
    void OnTriggerEnter(Collider newNeighbor)
    {
        if (newNeighbor.tag == "robot")
        {
            if (neighborList.Find(x => x == newNeighbor.gameObject) == null)
            {
                neighborList.Add(newNeighbor.gameObject);
            }
        }
    }

    // when any robot exits this robots collider sphere that robot is removed 
    // form the neighbor list
    void OnTriggerExit(Collider newNeighbor)
    {

        tempGO = neighborList.Find(x => x == newNeighbor.gameObject);
        if (tempGO != null)
        {
            neighborList.Remove(tempGO);
        }
    }
}
