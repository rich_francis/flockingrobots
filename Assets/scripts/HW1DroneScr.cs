﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HW1DroneScr : MonoBehaviour {

    public List<GameObject> neighborList; // dynamic list of neighbors
    public LineRenderer mainLine;
    public Material blueLine;
    public Material greenLine;
    public Color c1 = Color.blue;
    public Color c2 = Color.blue;
    int stringIndex = 0;
    string neighbourList;
    HWDriverSCr mainDriver;
    public int myIndex;

    // Use this for initialization
    void Start () {
        mainDriver = GameObject.Find("Main Camera").GetComponent<HWDriverSCr>();
        neighborList = new List<GameObject>(); // instantiated a list of neighbors on instantiation
        mainLine = GetComponent<LineRenderer>();
        mainLine.numPositions = 30;
        for(int i = 0; i < 30; i++)
        {
            mainLine.SetPosition(i, gameObject.transform.position);
        }

        neighbourList = gameObject.name + " :";
        mainDriver.updateNeighbourList(neighbourList, myIndex);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay2D(Collider2D newNeighbor)
    {
        if (neighborList.Find(x => x == newNeighbor.gameObject) == null)
        {
            neighborList.Add(newNeighbor.gameObject);
            setEdge(gameObject.transform.position, newNeighbor.gameObject.transform.position, stringIndex);
            neighbourList += newNeighbor.gameObject.name + ", ";
            mainDriver.updateNeighbourList(neighbourList, myIndex);
            stringIndex += 2;
        }
    }

    public void addNeighbor(GameObject N)
    {
        neighborList.Add(N);
        setEdge(gameObject.transform.position, N.gameObject.transform.position, stringIndex);
        neighbourList += N.gameObject.name + ", ";
        mainDriver.updateNeighbourList(neighbourList, myIndex);
        stringIndex += 2;
    }

    public void setEdge(Vector3 source, Vector3 dest, int lineIndex)
    {
        //mainLine.material = new Material(Shader.Find("Particles/Additive"));
        mainLine.startColor = c1;
        mainLine.endColor = c2;
        if (gameObject.name == "testDrone")
        {
            mainLine.material = greenLine;
        }
        else
        {
            mainLine.material = blueLine;
        }
        mainLine.startWidth = 0.4f;
        mainLine.endWidth = 0.4f;
        mainLine.SetPosition(lineIndex, source);
        lineIndex++;
        mainLine.SetPosition(lineIndex, dest);
        if (gameObject.name == "testDrone")
        {
            Debug.Log(lineIndex + " " + source);
            
            Debug.Log(lineIndex + " " + dest);
        }
    }
}
