﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class exportScr : MonoBehaviour {
    public GameObject[] allBots;
    string fileName = "MyFile.txt";
    StreamWriter sr;
    public List<float[]> velocityRecorder;
    int counter;

    // Use this for initialization
    void Start () {
        velocityRecorder = new List<float[]>();
        sr = File.CreateText(fileName);
        sr.WriteLine("This is my file.");
        sr.WriteLine("I can write ints {0} or floats {1}, and so on.",
            1, 4.2);
       sr.Close();

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("space"))
        {
            Debug.Log("data writing out");
            writeData();
        }

        if (counter %15 == 0)
        {
            float[] tempPos = new float[10];
            for (int i = 0; i < 10; i++)
            {
                tempPos[i] = allBots[i].GetComponent<Rigidbody2D>().velocity.magnitude;
            }
            velocityRecorder.Add(tempPos);
           // Debug.Log(velocityRecorder[velocityRecorder.Count-1][0] + " " + velocityRecorder[velocityRecorder.Count - 1][1]);
        }

        

        counter++;
    }

    public void writeData()
    {
        Debug.Log("Writing Data");
        sr = File.CreateText(fileName);
        for (int i = 0; i < velocityRecorder.Count; i++)
        {

                sr.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                    velocityRecorder[i][0], velocityRecorder[i][1],
                    velocityRecorder[i][2], velocityRecorder[i][3],
                    velocityRecorder[i][4], velocityRecorder[i][5],
                    velocityRecorder[i][6], velocityRecorder[i][7],
                    velocityRecorder[i][8], velocityRecorder[i][9]);
            
        }
        sr.Close();
    }
}
